FROM python:3.11-alpine
LABEL authors="Bengt Giger <bgiger@ethz.ch>"
LABEL maintainer="Bengt Giger <bgiger@ethz.ch>"
ENV PYTHONUNBUFFERED=1

COPY /src/test_in/main.py /app/main.py
COPY /data /app/data
COPY /requirements.txt /app/requirements.txt

RUN pip install --extra-index-url=https://gitlab.ethz.ch/api/v4/projects/45207/packages/pypi/simple -r /app/requirements.txt

RUN addgroup py && adduser -D -G py py
USER py:py

WORKDIR /app

CMD python3 main.py
